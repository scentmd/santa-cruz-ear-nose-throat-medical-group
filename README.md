We provide state of the art, caring treatment of ear, nose and throat diseases. Our care spans from sinus disease to head and neck cancer and from disorders of hearing and balance to pediatric issues including ear infections and tonsillitis.

Address: 243 Green Valley Rd, Suite B, Freedom, CA 95019

Phone: 831-724-9449
